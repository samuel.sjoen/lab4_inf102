package INF102.lab4.sorting;

import java.util.Collections;
import java.util.List;

public class BubbleSort implements ISort {
    // int scope;

    // @Override
    // public <T extends Comparable<T>> void sort(List<T> list) {
    // if (scope == 0) {
    // scope = list.size()-1;
    // }
    // for (int i = 0; i < scope; i++) {
    // T elem1 = list.get(i);
    // T elem2 = list.get(i + 1);

    // if (elem1.compareTo(elem2) > 0) {
    // list.set(i, elem2);
    // list.set(i + 1, elem1);
    // }
    // }
    // scope -= 1;
    // System.out.println(scope);
    // if (scope != 1) {
    // sort(list);
    // }

    // }

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {
        int scope = list.size() - 1;
        for (int iteration = 0; iteration < list.size(); iteration++) {
            for (int i = 0; i < scope; i++) {
                if (list.get(i).compareTo(list.get(i + 1)) > 0) {
                    Collections.swap(list, i, i+1);
                }
            }
            scope -= 1;
        }
    }
}