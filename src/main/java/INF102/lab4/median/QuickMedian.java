package INF102.lab4.median;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuickMedian implements IMedian {

    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        return medianOfMedians(listCopy, 0, listCopy.size());
    }

    private <T extends Comparable<T>> T medianOfMedians(List<T> list, int start, int end) {
        int size = list.size();
        int median = size / 2;
        T pivot = list.get(size);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).compareTo(pivot) > 0) {
                for (int j = size; j > 0; j--) {
                    if (list.get(j).compareTo(pivot) < 0) {
                        if (j > i) {
                            Collections.swap(list, i, j);
                        } else {
                            Collections.swap(list, i, size);
                            if (i == median) {
                                return list.get(i);
                            } else if (i < median) {
                                return medianOfMedians(list, i, list.size());
                            } else {
                                return medianOfMedians(list, 0, i);
                            }
                        }
                    }
                }
            }
        }
    }
}
    // Annet forsøk
    // @Override
    // public <T extends Comparable<T>> T median(List<T> list) {
    //     List<T> listCopy = new ArrayList<>(list); // Method should not alter list
    //     return medianOfMedians(listCopy, 0, listCopy.size());
    // }

    // private <T extends Comparable<T>> T medianOfMedians(List<T> list, int start, int end) {
    //     int indexOfPivot = 0;
    //     T pivot = list.get(indexOfPivot);
    //     Collections.swap(list, indexOfPivot, list.size() - 1);

    //     int indexOfFirstGreaterFromLeft = findFirstGreaterFromLeft(list, pivot, 0, list.size() - 2);
    //     int indexOfFirstLessFromRight = findFirstLessFromRight(list, pivot, list.size() - 2, 0);

    //     if (indexOfFirstGreaterFromLeft == -1 || indexOfFirstLessFromRight == -1) {
    //         return pivot;
    //     }
    //     T elemRight = list.get(indexOfFirstLessFromRight);
    //     T elemLeft = list.get(indexOfFirstGreaterFromLeft);

    //     if (elemLeft.compareTo(elemRight) < 0) {
    //         Collections.swap(list, indexOfFirstGreaterFromLeft, indexOfFirstLessFromRight);
    //     } else {
    //         findFirstGreaterFromLeft(list, pivot, start, end)
    //     }
    // }

    // private <T extends Comparable<T>> int findFirstGreaterFromLeft(List<T> list, T pivot, int startIndex,
    //         int lastIndex) {
    //     for (int i = startIndex; i <= lastIndex; i++) {
    //         if (list.get(i).compareTo(pivot) > 0)
    //             return i;
    //     }

    //     return -1;
    // }

    //     private <T extends Comparable<T>> int findFirstLessFromRight(List<T> list, T pivot, int startIndex, int lastIndex) {
    //     for (int i = startIndex; i >= lastIndex; i--) {
    //         if (list.get(i).compareTo(pivot) < 0)
    //             return i;
    //     }

    //     return -1;
    // }